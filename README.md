# fly-word-wall

# 弹幕墙 danmu app

[codepen 预览 online demo](http://codepen.io/ranforce/full/YpLBKE/)

[freecodecamp Design a danmu app](http://www.freecodecamp.cn/challenges/design-a-danmu-app) 教学任务。

实现了基于jQuery的简单的弹幕页面，有弹幕发射、弹幕清除功能。提供CSS3和jQuery animate 两种实现方式。实现弹幕在不同浏览器窗口间相互发送。

项目不支持文件（file:）直接打开，需要使用web服务器(http:// 或 https:// )，以src为根目录，打开index.html文件即可。
